const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const path = require('path');
const authRouter = require('./routers/authRouter');
const userNotesRouter = require('./routers/userNotesRouter');
const userRouter = require('./routers/userRouter');


const app = express();
const port = process.env.PORT || 8080;

const accessLogStream = fs.createWriteStream(
    path.join(__dirname, 'logs.log'), {flags: 'a'},
);


app.use(express.json());
app.use(morgan('tiny', {stream: accessLogStream}));

app.use('/api/auth', authRouter);
app.use('/api/notes', userNotesRouter);
app.use('/api/users/me', userRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://testuser:testuser1@cluster0.g8c2f.mongodb.net/homework2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });


  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();

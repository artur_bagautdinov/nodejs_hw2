const {JWT_SECRET} = require('../config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  try {
    const {username, password} = req.body;

    if (!username) {
      return res.status(400).json({message: 'No username found!'});
    } else if (!password) {
      return res.status(400).json({message: 'No password found!'});
    }

    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });


    await user.save();

    res.json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: `Internal server error`});
  }
};

module.exports.login = async (req, res) => {
  try {
    const {username, password} = req.body;

    const user = await User.findOne({username});

    if (!user) {
      return res.status(400).json(
          {message: `No user with username '${username}' found!`},
      );
    }

    if ( !(await bcrypt.compare(password, user.password)) ) {
      return res.status(400).json({message: `Wrong password!`});
    }


    const token = jwt.sign(
        {username: user.username, _id: user._id},
        JWT_SECRET,
    );
    res.json({message: 'Success', jwt_token: token});
  } catch (e) {
    res.status(500).json({message: `Internal server error`});
  }
};

const express = require('express');
const {nanoid} = require('nanoid');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {User} = require('../models/userModel');

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {skip = 0, limit = 5} = req.query;
    const {decoded} = req.user;

    if (skip > limit) {
      return res.status(400).json(
          {message: 'Skip integer can not be greater than limit'},
      );
    }

    const notes = await User.find(
        {_id: decoded._id},
        {notes: 1, _id: 0},
        [{$skip: skip, $limit: limit}],
    );

    if (!notes) {
      return res.status(400).json({message: 'Nothing found!'});
    }

    res.json(notes[0]);
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', authMiddleware, async (req, res) => {
  try {
    const {text} = req.body;
    const {decoded} = req.user;

    if (text.length === 0) {
      return res.status(400).json({message: 'Please fill in the text field!'});
    }

    const note = {
      _id: nanoid(),
      userId: decoded._id,
      completed: false,
      text,
      createdDate: new Date().toISOString(),
    };

    await User.updateOne(
        {_id: decoded._id},
        {
          $push: {notes: note},
        },
    );

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;
    const user = await User.find({_id: decoded._id});
    const note = user[0].notes.find((item) => item._id === req.params.id);

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    res.json({note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.put('/:id', authMiddleware, async (req, res) => {
  try {
    const {text} = req.body;
    const {decoded} = req.user;
    const {id} = req.params;

    const user = await User.find({_id: decoded._id});
    const activeNote = user[0].notes.find(
        (item) => item._id === id,
    );

    if (!activeNote) {
      return res.status(400).json({message: 'Note not found'});
    }

    await User.updateOne(
        {'_id': decoded._id, 'notes._id': id},
        {$set: {'notes.$.text': text}},
    );


    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/:id', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;
    const {id} = req.params;

    const user = await User.find({_id: decoded._id});
    const activeNote = user[0].notes.find(
        (item) => item._id === id,
    );

    if (!activeNote) {
      return res.status(400).json({message: 'Note not found'});
    }

    const isCompleted = activeNote.completed;

    await User.updateOne(
        {'_id': decoded._id, 'notes._id': id},
        {$set: {'notes.$.completed': !isCompleted}},
    );

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;
    const {id} = req.params;

    const user = await User.find({_id: decoded._id});
    const activeNote = user[0].notes.find(
        (item) => item._id === id,
    );

    if (!activeNote) {
      return res.status(400).json({message: 'Note not found'});
    }

    await User.updateOne(
        {'_id': decoded._id},
        {$pull: {'notes': {_id: id}}},
    );

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});


module.exports = router;

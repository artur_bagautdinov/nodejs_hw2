const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {User} = require('../models/userModel');

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;
    const userProfile = await User.find(
        {_id: decoded._id},
        {_id: 1, username: 1, createdDate: 1},
        {},
    );

    res.json({user: userProfile[0]});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;

    await User.findByIdAndDelete(
        {_id: decoded._id},
    );

    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});


router.patch('/', authMiddleware, async (req, res) => {
  try {
    const {decoded} = req.user;
    const {oldPassword, newPassword} = req.body;

    const user = await User.findOne({_id: decoded._id});


    if (!oldPassword || !newPassword) {
      return res.status(400).json({message: `Please specify password!`});
    }

    if (!(await bcrypt.compare(oldPassword, user.password))) {
      return res.status(400).json({message: `Wrong password!`});
    }

    await User.findByIdAndUpdate(
        {_id: decoded._id},
        {password: await bcrypt.hash(newPassword, 10)},
    );


    res.json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});


module.exports = router;
